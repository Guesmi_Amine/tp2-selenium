package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadConfig {
    Properties properties;

    public ReadConfig(){

        File fileSrc = new File("./src/test/java/Configuration/config.properties");

        try{
            FileInputStream fileInputStream = new FileInputStream(fileSrc);
            properties = new Properties();
            properties.load(fileInputStream);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public String getApplicationUrl(){
        String baseUrl = properties.getProperty("baseUrl");
        return baseUrl;
    }

    public String getChromeDriver(){
        return properties.getProperty("chromeDriverPath");
    }
}
