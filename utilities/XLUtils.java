package utilities;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class XLUtils {

    public static FileInputStream fileInputStream;
    public static FileOutputStream fileOutputStream;
    public static XSSFWorkbook workbook;
    public static XSSFSheet sheet;
    public static XSSFRow row;
    public static XSSFCell cell;

    public static int getRowCount(String xlfile,int indexSheet) throws IOException
    {
        fileInputStream = new FileInputStream(xlfile);
        workbook=new XSSFWorkbook(fileInputStream);
        sheet=workbook.getSheetAt(indexSheet);
        int rowcount=sheet.getLastRowNum();
        workbook.close();
        fileInputStream.close();
        return rowcount;
    }

    public static int getCellCount(String xlfile,int indexSheet,int rownum) throws IOException
    {
        fileInputStream = new FileInputStream(xlfile);
        workbook=new XSSFWorkbook(fileInputStream);
        sheet=workbook.getSheetAt(indexSheet);
        row=sheet.getRow(rownum);
        int cellCount = row.getLastCellNum();
        workbook.close();
        fileInputStream.close();
        return cellCount;
    }

    public static String getCellData(String xlfile,int indexSheet,int rownum,int colnum) throws IOException
    {
        fileInputStream = new FileInputStream(xlfile);
        workbook=new XSSFWorkbook(fileInputStream);
        sheet=workbook.getSheetAt(indexSheet);
        row=sheet.getRow(rownum);
        cell=row.getCell(colnum);
        String data;
        try
        {
            DataFormatter formatter = new DataFormatter();
            String cellData = formatter.formatCellValue(cell);
            return cellData;
        }
        catch (Exception e)
        {
            data="";
        }
        workbook.close();
        fileInputStream.close();
        return data;
    }

    public static void setCellData(String xlfile,int indexSheet,int rownum,int colnum,String data) throws IOException
    {
        fileInputStream = new FileInputStream(xlfile);
        workbook=new XSSFWorkbook(fileInputStream);
        sheet=workbook.getSheetAt(indexSheet);

        row=sheet.getRow(rownum);
        cell=row.createCell(colnum);
        cell.setCellValue(data);

        fileOutputStream=new FileOutputStream(xlfile);
        workbook.write(fileOutputStream);
        workbook.close();
        fileInputStream.close();
        fileOutputStream.close();
    }

}
